#ifndef SHIFTLOGIC_H
#define SHIFTLOGIC_H

// Digital output pins for LED light
const int ROW_SER_PIN = 9;  // Row serial pin
const int ROW_SRCLK_PIN = 10; // Row shift register pin

const int COL_SER_PIN = 11;  // Column serial pin
const int COL_SRCLK_PIN = 12; // Column shift register pin

const int RCLK_PIN = 13;  // Common latch pin for both LED shift registers

const int NUM_LIGHTS = 64; // 8x8

// Digital out pins for row switch (active low)
const int MAGROW_SER_PIN = 2;  // Row serial pin
const int MAGROW_SRCLK_PIN = 3; // Row shift register pin

// Digital input pins for magnetic sensors
const int MAGCOL_SER_PIN = 4;  // Column serial pin
const int MAGCOL_SRCLK_PIN = 5; // Column shift register pin

const int MAGRCLK_PIN = 6;  // Common latch pin for both MAG shift registers

const int NUM_MAGS = 64; // 8x8

void serialReadWrite(const bool data[NUM_LIGHTS], bool mags[NUM_MAGS]); // Writes the serial data to the pins defined above





#endif // SHIFTLOGIC_H