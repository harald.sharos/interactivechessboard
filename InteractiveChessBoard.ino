#include "ChessBoard.h" // Library for piece types and movement

ChessBoard board;
unsigned long elapsedTime, previousTime;

bool demoMode;
int demoRow1;
int demoRow2;
Column demoCol2;
int demoCycle;

const int MAG_DEBUG_PIN = A0;
int magDebugValue;
int magRead;

void setup()
{
  // LED pins
  pinMode(ROW_SER_PIN, OUTPUT);
  pinMode(ROW_SRCLK_PIN, OUTPUT);

  pinMode(COL_SER_PIN, OUTPUT);
  pinMode(COL_SRCLK_PIN, OUTPUT);

  pinMode(RCLK_PIN, OUTPUT);

  // magnetic sensor pins
  pinMode(MAGROW_SER_PIN, OUTPUT);
  pinMode(MAGROW_SRCLK_PIN, OUTPUT);

  pinMode(MAGCOL_SER_PIN, INPUT);
  pinMode(MAGCOL_SRCLK_PIN, OUTPUT);

  pinMode(MAGRCLK_PIN, OUTPUT);

  // Other setup

  pinMode(MAG_DEBUG_PIN, INPUT);

  Serial.begin(115200);

  board = ChessBoard::debugChessBoard();
  demoRow1 = 6;
  demoRow2 = 8;
  demoCol2 = Column::D;
  demoCycle = 0;
  magDebugValue = analogRead(MAG_DEBUG_PIN);
  demoMode = true;
}


void loop()
{

  if (millis() - previousTime > (demoMode ? 3000 : 200))
  {

    Serial.println("Demo cycle: " + String(demoCycle));
    previousTime = millis();
    if (demoMode) demo(board);
    Serial.println("Mag state: ");
    Serial.println(analogRead(MAG_DEBUG_PIN));

    if (magRead - magDebugValue > 10)
    {
      Serial.println("Magnet removed");
      board.pickUpPiece(Column::A, 8);

    }
    else if (magRead - magDebugValue < -10)
    {
      Serial.println("Magnet detected");
      board.movePiece(Column::A, 8, Column::A, 8);
    }
  magDebugValue = magRead;
  }
  magRead = analogRead(MAG_DEBUG_PIN);


  // board.refreshBoardLights();
  board.readWriteBoard();
}

void demo(ChessBoard& board)
{

  int newRow1;

  int newRow2;
  Column newCol2;

  switch(demoCycle)
  {
  case 0:
    board.pickUpPiece(Column::C, demoRow1);
    demoCycle++;

    break;
  
  case 1:
    newRow1 = (demoRow1 == 6) ? 5 : 6;
    board.movePiece(Column::C, demoRow1, Column::C, newRow1);
    demoRow1 = newRow1;
    
    demoCycle++;

    break;

  case 2:
    board.pickUpPiece(demoCol2, demoRow2);
    demoCycle++;

    break;

  case 3:

    if (demoRow2 == 8) {
      newRow2 = 5;
      newCol2 = Column::A;
    } else {
      newRow2 = 8;
      newCol2 = Column::D;
    }
    board.movePiece(demoCol2, demoRow2, newCol2, newRow2);
    demoRow2 = newRow2;
    demoCol2 = newCol2;
    demoCycle = 0;

    break;

  default:
    Serial.println("Demo cycle error");

    break;

  }
}
