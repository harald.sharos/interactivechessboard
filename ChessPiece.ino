#include "ChessPiece.h"

ChessPiece::ChessPiece():
  pieceType(PieceType::nothing),
  color(Color::nothing)
{
}

ChessPiece::ChessPiece(PieceType pieceType, Color color):
  pieceType(pieceType),
  color(color)
{

}

void ChessPiece::set(PieceType pieceType, Color color)
{
  this->pieceType = pieceType;
  this->color = color;
}

void ChessPiece::set(ChessPiece piece)
{
  this->pieceType = piece.pieceType;
  this->color = piece.color;
}

void ChessPiece::set(PieceType pieceType)
{
  this->pieceType = pieceType;
}

void ChessPiece::set(Color color)
{
  this->color = color;
}

PieceType ChessPiece::getPieceType() const
{
  return pieceType;
}

Color ChessPiece::getColor() const
{
  return color;
}


char* ChessPiece::toString() const
{
  /*
  * Returns the type of the piece as a string
  */
  switch (pieceType)
  {
    case PieceType::nothing:
      return "nothing";
    case PieceType::pawn:
      return "pawn";
    case PieceType::rook:
      return "rook";
    case PieceType::knight:  
      return "knight";  
    case PieceType::bishop:
      return "bishop";
    case PieceType::queen:
      return "queen";
    case PieceType::king:
      return "king";
    default:
      return "Unitialized piece.";
  }
}


int ChessPiece::getValue() const {
  /*
  * Returns the relative value of the board piece
  * Useful for calculating board position strength
  */
  switch (pieceType) {
    case PieceType::nothing:
      return 0;
    case PieceType::pawn:
      return 1;
    case PieceType::rook:
      return 5;
    case PieceType::knight:  
      return 3;  
    case PieceType::bishop:
      return 3;
    case PieceType::queen:
      return 9;
    case PieceType::king:
      return 0;
    default:
      return 0;
  }
}

