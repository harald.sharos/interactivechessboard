#ifndef CHESSBOARD_H
#define CHESSBOARD_H


#include "ChessPiece.h"
#include "BoardLogic.h"

enum class Column {A, B, C, D, E, F, G, H};

class ChessBoard {
public:
    ChessBoard();

    void setPiece(Column col, int row, ChessPiece piece);
    void movePiece(Column from_col, int from_row, Column to_col, int to_row);
    void pickUpPiece(Column col, int row);

    bool getLight(int idx) const;
    bool getMag(int idx) const;
    static ChessBoard debugChessBoard();

    void refreshBoardLights();
    void readWriteBoard();
private:
    ChessPiece boardPieces[NUM_LIGHTS];
    bool boardLights[NUM_LIGHTS];
    bool mags[NUM_MAGS];
    void resetBoard();
    void updateBoardLights();

    const ChessPiece& getPiece(int idx) const;
    
    void setLight(int idx, bool state);

    void setPiece(int idx, ChessPiece piece);

    void pawnLift(int i, int j);
    void pawnMove(int from_i, int from_j, int to_i, int to_j);

    void rookLift(int i, int j);
    void rookMove(int from_i, int from_j, int to_i, int to_j);

    void bishopLift(int i, int j);
    void bishopMove(int from_i, int from_j, int to_i, int to_j);



};

int col_int(Column col) 
{
    return (int)col;
}

int row_int(int row) 
{
    return 8-row;
}



#endif // CHESSBOARD_H