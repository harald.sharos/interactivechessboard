#include "ChessBoard.h"

ChessBoard::ChessBoard()
{
    this->resetBoard();
}

bool ChessBoard::getLight(int idx) const
{

  return boardLights[idx];
}

bool ChessBoard::getMag(int idx) const
{
  return mags[idx];
}

const ChessPiece& ChessBoard::getPiece(int idx) const
{
    return boardPieces[idx];
}

void ChessBoard::setLight(int idx, bool state)
{
    if (idx < 0 || idx > 63)
    {
        return;
    }
    boardLights[idx] = state;
}

void ChessBoard::setPiece(Column col, int row, ChessPiece piece)
{
    int i = row_int(row);
    int j = col_int(col);
    int idx = i*8 + j;
    this->setPiece(idx, piece);
}

void ChessBoard::setPiece(int idx, ChessPiece piece)
{
    boardPieces[idx] = piece;
}

void ChessBoard::resetBoard()
{
    for (int i = 0; i < 64; i++)
    {
        boardPieces[i] = ChessPiece();
    }
}

void ChessBoard::refreshBoardLights()
{
    for (int i = 0; i < 64; i++)
    {
      if (boardPieces[i].getPieceType() != PieceType::nothing)
      {
        boardLights[i] = true;
      }
      else
      {
        boardLights[i] = false;
      }
    }
}

void ChessBoard::readWriteBoard()
{
    serialReadWrite(boardLights, mags);
}


void ChessBoard::movePiece(Column from_col, int from_row, Column to_col, int to_row)
{
    // Move piece
    int from_i = row_int(from_row);
    int from_j = col_int(from_col);
    int to_i = row_int(to_row);
    int to_j = col_int(to_col);

    int from_idx = from_i*8 + from_j;
    int to_idx = to_i*8 + to_j;

    // Update lights
    switch (getPiece(from_idx).getPieceType()) {
        case PieceType::pawn:
            pawnMove(from_i, from_j, to_i, to_j);
            break;
        case PieceType::rook:
            rookMove(from_i, from_j, to_i, to_j);
            break;
        case PieceType::knight:
            setLight(to_idx, true);
            break;
        case PieceType::bishop:
            bishopMove(from_i, from_j, to_i, to_j);
            break;
        case PieceType::queen:
            setLight(to_idx, true);
            break;
        case PieceType::king:
            setLight(to_idx, true);
            break;
        default:
            printf("Tried to move 'nothing' piece on chessboard\n");
            Serial.println("Tried to move 'nothing' piece on chessboard");
            break;
    }
    if (from_idx != to_idx) {
      // Move piece if different location, else let it stay
      setPiece(to_idx, getPiece(from_idx));
      setPiece(from_idx, ChessPiece());
    }
}

void ChessBoard::pickUpPiece(Column col, int row)
{
  // Pick up piece
  int i = row_int(row);
  int j = col_int(col);
  int idx = i*8 + j;
  switch (getPiece(idx).getPieceType()) {
    case PieceType::pawn:
      pawnLift(i, j);
      break;
    case PieceType::rook:
      rookLift(i, j);
      break;
    case PieceType::knight:
      setLight(idx, false);
      break;
    case PieceType::bishop:
      bishopLift(i, j);
      break;
    case PieceType::queen:
      setLight(idx, false);
      break;
    case PieceType::king:
      setLight(idx, false);
      break;
    default:
      printf("Tried to pick up 'nothing' piece on chessboard\n");
      Serial.println("Tried to pick up 'nothing' piece on chessboard");
      break;
  }
}

void ChessBoard::pawnLift(int i, int j) {
  // Set given row and column to true
  setLight((i+1)*8 + j, true);
  // Set pawn position to false
  setLight(i*8 + j, false);
}

void ChessBoard::pawnMove(int from_i, int from_j, int to_i, int to_j) 
{
  // Set given row and column to false (after pawnLift)
  int possibleMove = (from_i+1)*8 + from_j;
  if (getPiece(possibleMove).getPieceType() == PieceType::nothing)
  {
    setLight(possibleMove, false);
  }
  // Set new pawn position to true
  setLight(from_i*8 + from_j, false);
  setLight(to_i*8 + to_j, true);
  
}

void ChessBoard::rookLift(int i, int j) {
  // Set given row and column to true
  for (int k = 0; k < 8; k++) {
    setLight(i*8 + k, true);
    setLight(k*8 + j, true);
  }
  // Set rook position to false
  setLight(i*8 + j, false);
}

void ChessBoard::rookMove(int from_i, int from_j, int to_i, int to_j) 
{
  // Set given row and column to false (after rookLift)
  for (int k = 0; k < 8; k++) 
  {
    if (getPiece(from_i*8 + k).getPieceType() == PieceType::nothing)
    {
      setLight(from_i*8 + k, false);
    }
    if (getPiece(k*8 + from_j).getPieceType() == PieceType::nothing)
    {
      setLight(k*8 + from_j, false);
    }
  }

  int from_idx = from_i*8 + from_j;
  int to_idx = to_i*8 + to_j;
  // Set new rook position to true
  setLight(from_idx, false);
  setLight(to_idx, true);
}

void ChessBoard::bishopLift(int i, int j) {
  // Set diagonals to true
  for (int k = 0; k < 8; k++) {
    setLight((i+k)*8 + (j-k), true);
    setLight((i+k)*8 + (j+k), true);
    setLight((i-k)*8 + (j-k), true);
    setLight((i-k)*8 + (j+k), true);
  }
  // Set bishop position to false
  setLight(i*8 + j, false);
}

void ChessBoard::bishopMove(int from_i, int from_j, int to_i, int to_j) 
{
  // Set diagonals to false (after bishopLift)
  for (int k = 0; k < 8; k++) 
  {
    int nw = (from_i+k)*8 + (from_j-k);
    int ne = (from_i+k)*8 + (from_j+k);
    int sw = (from_i-k)*8 + (from_j-k);
    int se = (from_i-k)*8 + (from_j+k);

    if (getPiece(nw).getPieceType() == PieceType::nothing)
    {
      setLight(nw, false);
    }
    if (getPiece(nw).getPieceType() == PieceType::nothing)
    {
      setLight(ne, false);
    }
    if (getPiece(sw).getPieceType() == PieceType::nothing)
    {
      setLight(sw, false);
    }
    if (getPiece(se).getPieceType() == PieceType::nothing)
    {
      setLight(se, false);
    }
  }

  int from_idx = from_i*8 + from_j;
  int to_idx = to_i*8 + to_j;
  // Set new bishop position to true
  setLight(from_idx, false);
  setLight(to_idx, true);
}

ChessBoard ChessBoard::debugChessBoard()
{
    ChessBoard board = ChessBoard();

    board.setPiece(Column::D, 8, {PieceType::bishop, Color::black});
    board.setPiece(Column::C, 6, {PieceType::rook, Color::white});
    board.setPiece(Column::A, 8, {PieceType::rook, Color::white});


    board.refreshBoardLights();
    return board;
}