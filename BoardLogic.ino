#include "BoardLogic.h"


void serialReadWrite(const bool data[NUM_LIGHTS], bool mags[NUM_MAGS])
{
  for (int grp = 0; grp < 8; grp++)
  {
    digitalWrite(RCLK_PIN, LOW); // Start latch period for both LED shift registers
    digitalWrite(MAGRCLK_PIN, LOW); // Start latch period for both MAG shift registers
    // Writes a single bit to the ROW register.
    // 0 means that group is active / on, 1 means off.
    // At the beginning of the loop, one 0 is shifted
    // onto the first bit, and later shifted upwards
    // as 1s turn the previous groups off.
    digitalWrite(ROW_SRCLK_PIN, LOW);
    digitalWrite(ROW_SER_PIN, grp == 0 ? 0 : 1);
    digitalWrite(ROW_SRCLK_PIN, HIGH);

    digitalWrite(MAGROW_SRCLK_PIN, LOW);
    digitalWrite(MAGROW_SER_PIN, HIGH);
    digitalWrite(MAGROW_SRCLK_PIN, HIGH);

    // Depending on the arrangement of the LEDs, and which
    // bits are conceptually most/least significant, shift in
    // increasing or reverse order.
    for(int colIdx = 7; colIdx > -1; colIdx--) 
    {
      digitalWrite(COL_SRCLK_PIN, LOW);
      bool bit = data[grp*8 + colIdx];
      digitalWrite(COL_SER_PIN, bit);
      digitalWrite(COL_SRCLK_PIN, HIGH);

      digitalWrite(MAGCOL_SRCLK_PIN, LOW);
      mags[grp*8 + colIdx] = digitalRead(MAGCOL_SRCLK_PIN);
      digitalWrite(MAGCOL_SRCLK_PIN, HIGH);
    }
    digitalWrite(RCLK_PIN, HIGH); // End latch period for both shift registers
    digitalWrite(RCLK_PIN, HIGH); // End latch period for both shift registers
  }
}