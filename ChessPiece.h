#ifndef PIECE_H
#define PIECE_H

enum class PieceType {nothing, pawn, rook, knight, bishop, queen, king};
enum class Color {nothing, white, black};

class ChessPiece {
private:
  PieceType pieceType = PieceType::nothing;
  Color color = Color::nothing;

public:
  ChessPiece();
  ChessPiece(PieceType pieceType, Color color);

  void set(PieceType pieceType, Color color);
  void set(ChessPiece piece);
  void set(PieceType pieceType);
  void set(Color color);

  PieceType getPieceType() const;
  Color getColor() const;
  char* toString() const;
  int getValue() const;

};


#endif // PIECE_H